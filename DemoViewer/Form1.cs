﻿using System;
using System.Windows.Forms;

using XPV;
using XPV.Core;

namespace DemoViewer
{
    public partial class Form1 : Form
    {
        public static ProfileData ProFile;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProFile = Manager.GetProfile(textBox1.Text);

            pictureBox1.Load(ProFile.PicLink);
            richTextBox1.AppendText($"GamerTag: {ProFile.GamerTag}\n");
            richTextBox1.AppendText($"GamerScore: {ProFile.GamerScore}\n");
            richTextBox1.AppendText($"MemberShip: {ProFile.Membership}\n");

            foreach (var game in ProFile.games)
            {
                listBox1.Items.Add(game.Name);
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1 || ProFile == null ||  listBox1.SelectedIndex > ProFile.games.Count)
                return;

            var index = listBox1.SelectedIndex;
            var game = ProFile.games[index];

            pictureBox2.Load(game.Picurl);
            label1.Text = $"Achievement: {game.Unlocked_achievement}/{game.Max_achievement}";
            label2.Text = $"Score: {game.Unlocked_Score}/{game.Max_Score}";

        }
    }
}
