﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace XPV.Core.Classes
{
    public partial class GameInfo
    {
        public string Name { get; private set; } = "PlacHolder";

        public string Picurl { get; private set; } 

        public int Unlocked_achievement { get; private set; } = 0;

        public int Max_achievement { get; private set; } = 0;

        public DateTime LastPlayed { get; private set; }

        public int Unlocked_Score { get; private set; } = 0;

        public int Max_Score { get; private set; } = 0;

        public GameInfo(HtmlNode node)
        {
            Name                 = node.ChildNodes[3].ChildNodes[1].InnerText;
            Picurl               = node.ChildNodes[1].ChildNodes[0].ChildNodes[0].Attributes[0].Value;
            Unlocked_achievement = Convert.ToInt32(node.ChildNodes[5].ChildNodes[3].ChildNodes[1].InnerText.Split('/')[0]);
            Max_achievement      = Convert.ToInt32(node.ChildNodes[5].ChildNodes[3].ChildNodes[1].InnerText.Split('/')[1]);
            Unlocked_Score       = Convert.ToInt32(node.ChildNodes[5].ChildNodes[1].ChildNodes[1].InnerText.Split('/')[0]);
            Max_Score            = Convert.ToInt32(node.ChildNodes[5].ChildNodes[1].ChildNodes[1].InnerText.Split('/')[1]);

            //LastPlayed = node.ChildNodes[3].ChildNodes[5].InnerText.Substring(15);

            //Console.WriteLine(node.ChildNodes[1].ChildNodes[0].ChildNodes[0].Attributes[0].Value);
            //Console.WriteLine("=========================================");
        }
    }
}
