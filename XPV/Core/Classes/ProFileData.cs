﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using XPV.Core.Classes;
using XPV.Core.Managers;

namespace XPV.Core
{
    public partial class ProfileData
    {
        public string GamerTag { get; private set; } = "Player";

        public int GamerScore { get; private set; } = 0;

        public string PicLink { get; private set; }

        public string Membership { get; private set; }

        public List<GameInfo> games { get; private set; }

        public ProfileData(HtmlDocument document)
        {
            if (document == null)
                return;

            games = new List<GameInfo>();

            var node = document.DocumentNode;

            GamerTag = node.SelectSingleNode("//*[@id=\"XGTMain\"]/div/h2").InnerText;
            GamerScore = Convert.ToInt32(node.SelectSingleNode("//*[@id=\"topLeft\"]/p[1]/text()").InnerText.Replace(",", String.Empty));
            PicLink = node.SelectSingleNode("//*[@id=\"topLeft\"]/img").Attributes[0].Value;
            Membership = node.SelectSingleNode("//*[@id=\"XGTMain\"]/div[3]/p").InnerText.Substring(17);

            foreach (var Gamenode in node.SelectSingleNode("//*[@id=\"recentGamesTable\"]").ChildNodes)
            {
                if (Gamenode.Name == "#text")
                    continue;

                games.Add(new GameInfo(Gamenode));
            }

            Console.WriteLine(games.Count);
        }
    }
}
