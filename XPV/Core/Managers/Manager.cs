﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPV.Core;
using XPV.Core.Managers;

namespace XPV
{
    public static class Manager
    {
        public static ProfileData GetProfile(string GT)
        {
            var node = WebHelper.GetDoc(GT);

            if (node == null)
                return null;

            ProfileData proFile = new ProfileData(node);

            return proFile;
        }
    }
}
