﻿using System;
using System.Net;
using HtmlAgilityPack;

namespace XPV.Core.Managers
{
    internal static class WebHelper
    {
        public static WebClient Client = new WebClient();

        public static string EndPoint = @"https://www.xboxgamertag.com/search/";

        public static HtmlDocument GetDoc(string GT)
        {
            if (GT.Length > 15)
                throw new ArgumentOutOfRangeException("Gametag cant be over 15 chars");

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(Client.DownloadString($@"{EndPoint}{GT}"));

            if (doc != null)
                return doc;

            return null;
        }
    }
}
